# Muscle Demo

A simple repo with two demos of muscles models linked with pinocchio.

## Installation
Install all dependencies using either anaconda (or mamba)
```
conda env create -f env.yaml
```
Then just activate it
```
conda activate muscle_demo
```

## Running the code
Once an example is running, a meshcat window will open. 

![](imgs/model.png)

It is possible to change the speed of the movement by changing the muscle control `u`.

```
u["bicLong"] = 0.6
u["bicShort"] = 0.5
u["BRA"] = 0.5
```

## TODO
- [ ] Add wrapping for muscle, in order to do arm extension
- [ ] Add some documentation
- [ ] Find a way to link control `u` to kinematics