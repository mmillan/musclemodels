import numpy as np
import pinocchio as pin
import meshcat.geometry as g

class Point:
    def __init__(self, coord, name, attachment, model, parent=None, object=None):
        self.attachment = attachment
        self.name = name
        self.parent = parent
        model.addFrame(pin.Frame(name, model.getJointId(attachment[0]), model.getFrameId(attachment[1]), coord, pin.FrameType.OP_FRAME))

        self.object = object

    def compute_path(self, model, data):
        if self.object == None:
            return np.linalg.norm(data.oMf[model.getFrameId(self.name)].actInv(data.oMf[model.getFrameId(self.parent)].translation)) 
        else:
            p1 = data.oMf[model.getFrameId(self.object.name)].actInv(data.oMf[model.getFrameId(self.parent)]).translation
            p2 = data.oMf[model.getFrameId(self.object.name)].actInv(data.oMf[model.getFrameId(self.name)]).translation
            self.object.compute_path(p1, p2)
            path_length = self.object.path_length
            path_length += np.linalg.norm(p1 - self.object.wrap_pts[0])
            for p in range(1, len(self.object.wrap_pts)-1):
                path_length += np.linalg.norm(self.object.wrap_pts[p-1] - self.object.wrap_pts[p])

            path_length += np.linalg.norm(p2 - self.object.wrap_pts[-1])
        
            return path_length
    
    def compute_vector(self, model, data):
        if self.object is None:
            vec = data.oMf[model.getFrameId(self.name)].actInv(data.oMf[model.getFrameId(self.parent)]).translation
        else:
            pts = pin.SE3(np.eye(3), np.array(self.object.wrap_pts[-1]))
            vec = data.oMf[model.getFrameId(self.name)].actInv(data.oMf[model.getFrameId(self.object.name)].act(pts)).translation

        self.vec = vec / np.linalg.norm(vec)

        return self.vec
    
    def draw(self, model, data, viz):
        sphere = g.Sphere(0.002)
        viz.viewer[self.name].set_object(sphere)
        # Set the sphere's pose (position and orientation)
        pos = data.oMf[model.getFrameId(self.name)]
        T = np.r_[np.c_[pos.rotation, pos.translation], [[0, 0, 0, 1]]]
        viz.viewer[self.name].set_transform(T)



