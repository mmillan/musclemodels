import numpy as np

class ActivationDynamics(object):
    """
        Activation Dynamics of muscles.

        Attributes
        ----------
        dt : double
        Period of muscle updating in second
    """

    def __init__(self, dt):
        self.t_act = 0.010 
        self.t_deact = 0.040

        self.dt = dt
        self.amin = 0.0
        self.activation = self.amin

    def step(self, u):
        """
            Step of activation depending of control u. 
            Update activation based on da/dt = (u - a)/T(a, u) with T begin a time constant depending on both activation and control value.

            Input
            ----------
            u : double
            Excitation of muscle. Bound between 0 and 1.
        """
        # test over control
        if u < 0:
            print("Excitation is bound between 0 and 1")
            u = 0
        if u > 1:
            print("Excitation is bound between 0 and 1")
            u = 1

        self.activation = (self.activation - self.amin) / (1 - self.amin)
        if u <= self.activation:
            tau = self.t_deact/(0.5 + 1.5*self.activation)
        else:
            tau = self.t_act*(0.5 + 1.5*self.activation)

        self.activation += self.dt*(u - self.activation) / tau 

        if self.activation > 1:
            self.activation = 1
        elif self.activation < self.amin:
            self.activation = self.amin
        return self.activation
    
    def reinitialize(self):
        self.activation = 0