import numpy as np
import pinocchio as pin
import meshcat.geometry as g

from .points import Point

class MuscleGeometry(object):
    def __init__(self, names, points, origins, model, pen_angle, tendon_slack, opt_len, parents):
        self.points = []
        temp_point = Point(points[0], names[0], origins[0], model, parents[0])
        self.points.append(temp_point)
        for k in range(1, len(names)):
            # temp_id = model.addFrame(pin.Frame(names[k], model.getJointId(origins[k][0]), model.getFrameId(origins[k][1]), points[k], pin.FrameType.OP_FRAME))
            temp_point = Point(points[k], names[k], origins[k], model, parents[k], None)
            self.points.append(temp_point)

        self.joints = [origins[0][0], origins[-1][0]]
        self.pen_angle = pen_angle
        self.parallelogram_height = opt_len * np.sin(pen_angle)
        self.Lts = tendon_slack
        self.Lt = tendon_slack
        self.dLmt = 0
        self.dLt = 0

        self.objects = []

    def add_object(self, model, data):
        # put everything in the same frame
        for k in range(1, len(self.points)):
            self.points[k].object = None
            for o in self.objects:
                p1 = data.oMf[model.getFrameId(self.points[k].parent)].translation
                p2 = data.oMf[model.getFrameId(self.points[k].name)].translation
                if o.is_object_on_path(p1, p2, model, data):
                    self.points[k].object = o
    
    def calc_Lm(self):
        self.LmAT = self.Lmt - self.Lt #### Muscle Length along tendon
        self.Lm = np.sqrt(self.parallelogram_height**2 + self.LmAT**2)

    def calc_dLm(self):
        self.dLm = (self.dLmt - self.dLt) * np.cos(self.pen_angle)

    def calc_Lmt(self, model, data):
        self.Lmt = 0
        self.add_object(model, data)
        for k in range(1, len(self.points)):
            # self.Lmt += np.linalg.norm(data.oMf[model.getFrameId(self.points[k+1])].actInv(data.oMf[model.getFrameId(self.points[k])].translation))
            self.Lmt += self.points[k].compute_path(model, data)


    def calc_dLmt(self, model, data):
        self.dLmt = 0
        temp_dist = 0
        for k in range(1, len(self.points)):
            # temp_dist += np.linalg.norm(data.oMf[model.getFrameId(self.points[k+1])].actInv(data.oMf[model.getFrameId(self.points[k])].translation))
            temp_dist += self.points[k].compute_path(model, data)

        self.dLmt = (temp_dist - self.Lmt)
        self.Lmt = temp_dist
    
    def calc_jacobian(self, model, data, q):
        #find if there is a wrapping
        count = len(self.points)

        self.jacobian = np.zeros((3*count, model.nq))
        for k in range(len(self.points)):
            j = pin.computeFrameJacobian(model, data, q, model.getFrameId(self.points[k].name), pin.ReferenceFrame.LOCAL_WORLD_ALIGNED)
            self.jacobian[3*k:3*(k+1), :] = j[:3, :]
            
    def calc_length_jacobian(self, model, data, q):
        #find if there is a wrapping
        self.length_jacobian = np.zeros((1, model.nq))

        for k in range(len(self.points) - 1):
            dist = data.oMf[model.getFrameId(self.points[k+1].name)].translation - data.oMf[model.getFrameId(self.points[k].name)].translation
            leng = np.linalg.norm(dist)
            dist = dist @ (self.jacobian[3*(k+1):3*(k+2)] - self.jacobian[3*(k):3*(k+1)]) / leng
            self.length_jacobian += dist

    def calc_vector(self, model, data):
        #compute vector between the last 2 points ~ almost like the tendon direction
        self.vector_insertion = self.points[-1].compute_vector(model, data)
        self.vector_origin = self.points[1].compute_vector(model, data)
        # vec = data.oMf[model.getFrameId(self.points[-1])].actInv(data.oMf[model.getFrameId(self.points[0].name)].translation)
        # self.vector_insertion = vec / np.linalg.norm(vec)

        # vec = data.oMf[model.getFrameId(self.points[0])].actInv(data.oMf[model.getFrameId(self.points[-1].name)].translation)
        # self.vector_origin = vec / np.linalg.norm(vec)

    def draw(self, model, data, viz, name, color=0xFF4433):
        points_ = []
        for k in range(len(self.points)):
            if self.points[k].object != None:
                points_ = self.points[k].object.draw(model, data, viz, points_)
            temp_point = np.zeros((3,))
            temp_point = data.oMf[model.getFrameId(self.points[k].name)].translation
            self.points[k].draw(model, data, viz)
            points_.append(temp_point)
                    
        points_ = np.array(points_)
        viz.viewer[name].set_object(g.Line(g.PointsGeometry(points_.T), g.LineBasicMaterial(linewidth = 3.0, color=color, reflectivity=1)))

        #draw vector forces
        # p_insertion = [data.oMf[model.getFrameId(self.points[-1].name)].translation, data.oMf[model.getFrameId(self.points[-1].name)].act(pin.SE3(np.eye(3), self.points[-1].vec)).translation]
        # print(p_insertion)
        # print(data.oMf[model.getJointId(self.joints[1])].actInv(pin.SE3(np.eye(3), p_insertion[1])).translation)
        # viz.viewer[name + "vector_insertion"].set_object(g.Line(g.PointsGeometry(np.array(p_insertion).T), g.LineBasicMaterial(linewidth=3.5, color=0x89CFF0, reflectivity=1)))

    def update_geometry(self, model, data, q, viz, name):
        self.calc_Lmt(model, data)
        self.calc_Lm()
        self.calc_dLmt(model, data)
        #self.calc_vector(model, data)
        self.calc_jacobian(model, data, q)
        self.calc_length_jacobian(model, data, q)
        self.draw(model, data, viz, name)