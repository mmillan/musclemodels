from .muscle_geometry import MuscleGeometry 
from .activation import ActivationDynamics

import numpy as np

class Muscle:
    def __init__(self, mif, opt_l, tendon_slack, pen_angle, names, points, origins, model, dt, name_, parents):
        self.F_max = mif #Maximal isometric force 
        self.Lm_opt = opt_l #Length 
        self.V_max = 10.0

        self.geom = MuscleGeometry(names, points, origins, model, pen_angle, tendon_slack, opt_l, parents)
        self.activ = ActivationDynamics(dt)

        self.name = name_

    def update_muscle(self, u):
       pass 

class EquilibriumMuscle(Muscle):
    def __init__(self, mif, opt_l, tendon_slack, pen_angle, names, points, origins, model, dt, name_, parents):
        super().__init__( mif, opt_l, tendon_slack, pen_angle, names, points, origins, model, dt, name_, parents)

        self.V_opt = self.Lm_opt * np.sin(pen_angle)
        self.Af = 0.25
        self.Flen = 1.4
        self.fv_lin_threshold = 0.95

        # constant for every muscle
        self.kal = 0.45

        self.e0m = .6
        self.kpe = 5.0

        self.e0t = 0.04
        self.const_fse_1 = np.exp(0.3e1)
        self.kToe = 3.0
        self.FToe = 33.0/100.0
        self.eToe = (0.99e2*self.e0t*self.const_fse_1) / (0.166e3*self.const_fse_1 - 0.67e2)
        self.klint = (0.67e2/0.100e3) * 1.0/(self.e0t - (0.99e2*self.e0t*self.const_fse_1) / (0.166e3*self.const_fse_1 - 0.67e2))
        
    def calc_fal(self):
        len_norm = self.geom.Lm/self.Lm_opt
        self.fal = np.exp(-(len_norm-1)**2/self.kal)

    def calc_dfal_dLmN(self):
        len_norm = self.geom.Lm/self.Lm_opt
        self.dfal_dLmN = -2*len_norm/self.kal * np.exp(-(len_norm-1)**2/self.kal)

    def calc_fpe(self):
        len_normalized = self.geom.Lm/self.Lm_opt
        self.fpe = 0
        if len_normalized > 1:
            t5 = np.exp(self.kpe * (len_normalized - 1) / self.e0m)
            t7 = np.exp(self.kpe)
            self.fpe = (t5 - 1) / (t7 - 1)

    def calc_dfpe_dLmN(self):
        len_normalized = self.geom.Lm/self.Lm_opt
        self.dfpe_dLmN = 0
        if len_normalized > 1:
            t5 = np.exp(self.kpe * (len_normalized - 1) / self.e0m)
            t7 = np.exp(self.kpe)
            self.dfpe_dLmN = (self.kpe/self.e0m * t5) / (t7 - 1)

    def calc_dLmN(self, act, fal, a_fal_fv):
        dLm = 0
        dLm_dFm = 0
        b = 0
        db = 0

        a_fal = act * fal
        Fm_asyC = 0 #Concentric Contraction Asymptote
        Fm_asyE = act * fal
        asyE_thresh = self.fv_lin_threshold

        if(a_fal_fv > Fm_asyC and a_fal_fv < Fm_asyE*asyE_thresh):
            if(Fm <= a_fal): # M uscle is concentrically contracting
                b = a_fal + a_fal + Fm / self.Af
            else:  # Muscle is eccentrically contracting
                b = ((2 + 2/self.Af)*(a_fal*self.Flen - a_fal_fv)) / (self.Flen - 1)
                db = -(2 + 2/self.Af) / (self.Flen - 1)

            dLm = (0.25 + 0.75 * act)*(a_fal_fv - a_fal) / b
        else:
            Fm0 = 0
            # Compute d and db/dFm from Eqn 7. of Thelen2003
            if(a_fal_fv <= Fm_asyC): # Concentric Contraction
                Fm0 = Fm_asyC
                b = a_fal + Fm0 / self.Af
                db = 1/self.Af
            else: # Excentric Contraction
                Fm0 = Fm_asyE * asyE_thresh
                b = ((2 + 2/self.Af)*(a_fal*self.Flen - Fm0)) / (self.Flen - 1)
                db = -(2 + 2/self.Af) / (self.Flen - 1)

            dLm = (0.25 + 0.75 * act)*(Fm0 - a_fal) / b
            dLm_dFm = (0.25 + 0.75*act)/b - ((0.25 + 0.75*act)*(Fm0-a_fal)/b**2)*db
            dLm += dLm_dFm *(a_fal_fv - Fm0)
        
        return dLm
            
    def calc_dLmN_da_fal_fv(self, act, fal, a_fal_fv):
        dLm = 0
        dLm_dFm = 0
        b = 0
        db = 0

        a_fal = act * fal
        Fm_asyC = 0 #Concentric Contraction Asymptote
        Fm_asyE = act * fal
        asyE_thresh = self.fv_lin_threshold

        if(a_fal_fv > Fm_asyC and a_fal_fv < Fm_asyE*asyE_thresh):
            if(Fm <= a_fal): # Muscle is concentrically contracting
                b = a_fal + a_fal + Fm / self.Af
            else:  # Muscle is eccentrically contracting
                b = ((2 + 2/self.Af)*(a_fal*self.Flen - a_fal_fv)) / (self.Flen - 1)
                db = -(2 + 2/self.Af) / (self.Flen - 1)

            dLm_dFm = (0.25 + 0.75*act)/b - ((0.25 + 0.75*act)*(a_fal_fv-a_fal)/b**2)*db
        else:
            Fm0 = 0
            # Compute d and db/dFm from Eqn 7. of Thelen2003
            if(a_fal_fv <= Fm_asyC): # Concentric Contraction
                Fm0 = Fm_asyC
                b = a_fal + Fm0 / self.Af
                db = 1/self.Af
            else: # Excentric Contraction
                Fm0 = Fm_asyE * asyE_thresh
                b = ((2 + 2/self.Af)*(a_fal*self.Flen - Fm0)) / (self.Flen - 1)
                db = -(2 + 2/self.Af) / (self.Flen - 1)

            dLm_dFm = (0.25 + 0.75*act)/b - ((0.25 + 0.75*act)*(Fm0-a_fal)/b**2)*db
        
        return dLm_dFm
    
    def calc_fv_inv(self, max_iter, tol):
        ferr = 1
        iter = 0
        a_fal_fv = self.fv*self.activ.activation*self.fal
        delta_a_fal_fv = 0
        dLm1 = 0
        dLm1_dFm = 0

        while(abs(ferr) >=tol and iter < max_iter):
            dLm1 = self.calc_dLmN(self.activ.activation, self.fal, a_fal_fv)
            ferr = dLm1 - self.geom.dLm
            dLm1_dFm = self.calc_dLmN_da_fal_fv(self.activ.activation, self.fal, a_fal_fv)

            if(abs(dLm1_dFm) > 1e-6):
                delta_a_fal_fv = -ferr / dLm1_dFm
                a_fal_fv += delta_a_fal_fv
            
            iter+= 1

        if(abs(ferr) < tol):
            self.fv = max(0.0, a_fal_fv/self.activ.activation/self.fal)
        else:
            self.fv = 1

    def calc_FmAT(self):
        self.FmAT = self.F_max*(self.activ.activation*self.fal*self.fv + self.fpe)*np.cos(self.geom.pen_angle)
    
    def calc_dFm_dLm(self):
        self.calc_dfal_dLmN()
        self.calc_dfpe_dLmN()

        self.dFm_dLm = self.F_max/self.Lm_opt * (self.activ.activation*self.fv*self.dfal_dLmN + self.dfpe_dLmN)
    
    def calc_dFmAT_dLm(self):
        self.calc_dFm_dLm()
        self.dFmAT_dLm = self.dFm_dLm*np.cos(self.geom.pen_angle)

    def calc_fse(self):
        LtN = self.geom.Lt / self.geom.Lts
        x = LtN - 1
        self.fse = 0

        if (x > self.eToe):
            self.fse = self.klint*(x-self.eToe)+self.FToe
        elif (x > 0.0):
            self.fse =(self.FToe/(np.exp(self.kToe)-1.0))*(np.exp(self.kToe*x/self.eToe)-1.0)

    def calc_dfse_dLtN(self):
        LtN = self.geom.Lt / self.geom.Lts
        x = LtN - 1

        self.dfse_dLtN = 0

        if x > self.eToe:
            self.dfse_dLtN = self.klint
        elif x > 0:
            self.dfse_dLtN = (self.FToe / (np.exp(self.kToe) - 1)) * (self.kToe / self.eToe) * (np.exp(self.kToe*x / self.eToe))

    def calc_dfse_dLt(self):
        self.calc_dfse_dLtN()
        self.dfse_dLt = self.dfse_dLtN * (self.F_max/self.geom.Lts)

    def calc_dfse_dLm(self):
        self.calc_dfse_dLtN()
        LtN = self.geom.Lt / self.geom.Lts

        self.dtl_dLm = -np.cos(self.geom.pen_angle)
        self.dfse_dLm = self.dfse_dLtN * self.dtl_dLm*self.F_max/self.geom.Lts

    def calc_Ft(self):
        self.Ft = self.F_max * self.fse

    def compute_multipliers(self):
        self.calc_fal()
        self.calc_fpe()
        self.calc_fse()

    def compute_derivatives(self):
        self.calc_dFm_dLm()
        self.calc_dFmAT_dLm()
        self.dFmAT_dLmAT = self.dFmAT_dLm*np.cos(self.geom.pen_angle)

        self.calc_dfse_dLt()
        self.calc_dfse_dLm()

    def compute_velocities(self):
        ################## Taken from Opensim directly
        # Update velocity level quantities: share the muscle velocity 
        # //between the tendon and the fiber according to their relative 
        # //stiffness:
        # //
        # //Fm = Ft at equilibrium
        # //Fm = Km*lceAT
        # //Ft = Kt*xt
        # //dFm_d_xm = Km*dlceAT + dKm_d_t*lceAT (assume dKm_d_t = 0)
        # //dFt_d_xt = Kt*dtl + dKt_d_t*dtl (assume dKt_d_t = 0)
        # //
        # //This is a heuristic. The above assumptions are necessary as 
        # //computing the partial derivatives of Km or Kt w.r.t. requires 
        # //acceleration level knowledge, which is not available in 
        # //general.

        # //Stiffness of the muscle is the stiffness of the tendon and the 
        # //fiber (along the tendon) in series

        # //the if statement here is to handle the special case when the
        # //negative stiffness of the fiber (which could happen in this
        # // model) is equal to the positive stiffness of the tendon.
        if(abs(self.dFmAT_dLmAT + self.dfse_dLt) > 1e-6 and self.geom.Lt > self.geom.Lts):
            self.geom.dLt = (self.dFmAT_dLmAT + self.dfse_dLt) * self.geom.dLmt
        else:
            self.geom.dLt = self.geom.dLmt

        self.geom.calc_dLm()
        self.geom.dLmN = self.geom.dLm / (self.V_max * self.Lm_opt)
        self.calc_fv_inv(100, 0.05)

    def init_muscle(self, max_iter, tol):
        self.geom.Lt = self.geom.Lts * 1.01
        self.geom.calc_Lm()
        #init all quantities
        self.compute_multipliers()
        self.fv = 1.0
        self.compute_derivatives()
        self.compute_velocities()
        # Compute Error
        self.calc_FmAT()
        self.calc_Ft()
        ferr = self.FmAT - self.Ft
        # Compute derivatives again now that fv is updated
        self.compute_derivatives()

        iter = 0
        ferr_prec = ferr
        Lm_prec = self.geom.Lm
        while(abs(ferr) > tol and iter < max_iter):
            dferr_dLm = self.dFmAT_dLm - self.dfse_dLm
            h= 1.0

            while(abs(ferr) >= abs(ferr_prec)):
                delta_Lm = -h * ferr_prec / dferr_dLm
                if(abs(delta_Lm) > 1e-6):
                    self.geom.Lm = Lm_prec + delta_Lm
                else:
                    self.geom.Lm = Lm_prec - np.sign(delta_Lm)* 1e-8 
                    h = 0.

                # if self.geom.Lm < self.geom.min_len:
                #     self.geom.Lm = self.geom.min_len
                
                #Recompute position 
                self.geom.Lt = self.geom.Lmt - self.geom.Lm * np.cos(self.geom.pen_angle)

                self.compute_multipliers()
                
                # Compute Error
                self.calc_FmAT()
                self.calc_Ft()
                ferr = self.FmAT - self.Ft
                if h <= 1e-8:
                    break
                else:
                    h *= 0.5
            ferr_prec = ferr
            Lm_prec = self.geom.Lm

            self.compute_derivatives()
            self.compute_velocities()

            iter += 1

        self.compute_multipliers()
        self.compute_derivatives()
        self.compute_velocities()
        self.calc_Ft()

    def update_muscle(self, u):
        self.activ.step(u)
        self.init_muscle(100, 0.05)

        return self.Ft * self.geom.vector_origin, self.Ft * self.geom.vector_insertion

class RigidTendonMuscle(Muscle):
    def __init__(self, mif, opt_l, tendon_slack, pen_angle, names, points, origins, model, dt, name_, parents):
        super().__init__( mif, opt_l, tendon_slack, pen_angle, names, points, origins, model, dt, name_, parents)

        # constant for every muscle
        self.fal_1 = 0.15
        self.fal_2 = 0.45

        self.fv_1 = 1.0
        self.fv_2 = -.33/2 * self.fv_1/(1+self.fv_1)

        self.fpe_1 = 10.0
        self.fpe_2 = 5.0

    ############# see later for automatic differentiation
    def calc_fal(self):
        len_normalized = self.geom.Lm / self.Lm_opt
        self.fal = np.exp(-((len_normalized)/(self.fal_1*(1-self.activ.activation)+1) -1.0)**2 / self.fal_2)
    
    def calc_fv(self):
        self.geom.calc_dLm()
        v = self.geom.dLm
        if v <= 0:
            self.fv = (1.0-np.abs(v)/self.V_max)/(1+np.abs(v)/self.V_max/self.fv_1)
        else:
            self.fv = (1.0-1.33*v/self.V_max/self.fv_2)/(1-v/self.V_max/self.fv_2)

    def calc_fpe(self):
        len_normalized = self.geom.Lm/self.Lm_opt
        self.fpe = 0
        if self.geom.Lm > 0:
            self.fpe = np.exp(self.fpe_1*(len_normalized-1)-self.fpe_2)

    def calc_Fm(self):
        self.calc_fal()
        self.calc_fv()
        self.calc_fpe()
        self.Fm = self.F_max*(self.activ.activation*self.fal*self.fv + self.fpe)*np.cos(self.geom.pen_angle)      

    def update_muscle(self, u):
        self.activ.step(u)
        self.calc_Fm()
        return self.Fm
        # return self.Fm * self.geom.vector_origin, self.Fm * self.geom.vector_insertion
    
    def reinitialize(self):
        self.Fm = 0
        self.activ.reinitialize()