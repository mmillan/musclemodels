import numpy as np
import meshcat.geometry as g
from pinocchio import SE3
import pinocchio as pin

from scipy.spatial.transform import Rotation as R


class WrapObject:
    def __init__(self, position, parent_joint, model, axis, sign, name, muscle_name):
        #positon in the parent joint
        self.position = position
        self.parent = parent_joint
        self.name = name
        self.axis = axis # 0=x, 1=y, 2=z
        # sign positive or negative depending on the side or zero if muscle can wrap everywhere
        self.sign = sign

        self.muscle = muscle_name

        model.addFrame(pin.Frame(name, model.getJointId(parent_joint[0]), model.getFrameId(parent_joint[1]), position, pin.FrameType.OP_FRAME))

    def normalize(self, v1):
        magnitude = np.linalg.norm(v1)
        if magnitude < 1e-5:
            return np.zeros(v1.shape)
        else:
            return v1 / magnitude
    
    def closest_point_to_line(self, p, p_line, line):
        v1 = self.normalize(p - p_line)
        v2 = self.normalize(line)
        t = np.dot(v1, v2)*np.linalg.norm(p - p_line)
        # t = np.dot(np.cross(v1, v2), np.linalg.norm(v1))

        # Calculate closestPt
        p_closest = p_line + t * v2
        t /= np.linalg.norm(v2)

        return p_closest, t
    
    def squared_distance_point_line(self, p, p_line, line):
        p_pLine = p_line - p
        n = self.normalize(line)
        return np.linalg.norm(p_pLine - np.dot(p_pLine, n) * n)**2

    def intersect_lines(self, p11, p12, p21, p22):
        """
        Compute intersection between line p11-p12 and line p21-p22. 
        If lines are are parallel will return Falsen otherwise return 
        True, p_inter_1 (point on the first line which is closest to 2nd), 
        p_inter_2 (point on the 2nd closer to the first), s (distance from p11 to p_inter_1) and
        t (distance from p21 to p_inter_2)
        """
        mag1 = np.linalg.norm(p12 - p11)
        mag2 = np.linalg.norm(p22 - p21)
        vec1 = self.normalize(p12 - p11)
        vec2 = self.normalize(p22 - p21)

        cross_prod = np.cross(vec1, vec2)
        d = np.linalg.norm(cross_prod)**2
        if d < 1e-6:
            return False, np.array([0,0,0]), np.array([0,0,0]), 0, 0

        mat = np.array([p21 - p11, vec1, cross_prod])
        t = np.linalg.det(mat) / d

        p_inter_2 = p21 + t * vec2
        mat[1, :] = vec2
        s = np.linalg.det(mat) / d
        p_inter_1 = p11 + s * vec1

        s /= mag1
        t /= mag2

        return True, p_inter_1, p_inter_2, s, t 

    def intersect_line_plane(self, p1, p2, plane_n, d):
        vec = p2 - p1
        if(np.abs(np.dot(vec, plane_n)) < 1e-5):
            return False, np.zeros((3,))
        
        t = (-d -plane_n[0]*p1[0] -plane_n[1]*p1[1] -plane_n[2]*p1[2]) / np.dot(vec, plane_n)
        if(t < -1e-5 or t > 1 + 1e-5):
            return False, np.zeros((3,))
        
        inter = p1 + t * vec
        return True, inter
    
    def compute_path(self):
        pass

    def is_object_on_path(self, p1, p2):
        return False
    
class WrapSphere(WrapObject):
    def __init__(self, position, parent_joint, model, axis, sign, name, muscle_name, radius):
        super().__init__(position, parent_joint, model, axis, sign, name, muscle_name)
        self.radius = radius
        
    def compute_path(self, p1, p2):
        origin = self.position.translation
        far_side_wrap = False
        self.wrap_pts = []


        p1m = p1 - origin
        p2m = p2 - origin
        ri = p1 - p2
        mp = origin - p2
        p1p2 = p1 - p2

        #check points are not inside the sphere
        if(np.linalg.norm(p1m) < self.radius or np.linalg.norm(p2m) < self.radius):
            return np.linalg.norm(p1m - p2m)
        
        a = np.dot(ri.T, ri)
        b = -2.0 * np.dot(mp.T, ri)
        c = np.dot(mp.T, mp) - self.radius**2
        disc = b**2 - 4.0*a*c

        if disc < 0:
            return 0
        
        l1 = (-b + np.sqrt(disc)) / (2.0 * a)
        l2 = (-b - np.sqrt(disc)) / (2.0 * a)

        if not (0.0 < l1 < 1.0) or not (0.0 < l2 < 1.0):
            return 0
        
        if(l1 < l2):
            return 0
        
        p1p2 = self.normalize(p1p2)
        np2 = self.normalize(p2m)

        hp2 = np.cross(p1p2, np2)

        if np.linalg.norm(hp2) < 1e-5:
            # go to calc_path
            pass

        n = self.normalize(hp2)
        y = origin - p1
        y = self.normalize(y)
        z = np.cross(n, y)
        
        ra = np.zeros((3, 3))
        for i in range(3):
            ra[i][0] = n[i]
            ra[i][1] = y[i]
            ra[i][2] = z[i]

        a1 = np.arcsin(self.radius / np.linalg.norm(p1m))
        # Define rrx and aa
        rrx = np.zeros((3, 3))
        aa = np.zeros((3, 3))

        rrx[0][0] = 1.0
        rrx[1][1] = np.cos(a1)
        rrx[1][2] = -np.sin(a1)
        rrx[2][1] = np.sin(a1)
        rrx[2][2] = np.cos(a1)

        aa = np.dot(ra, rrx.T)

        r1a = np.zeros(3)
        for i in range(3):
            r1a[i] = p1[i] + aa[i][1] * np.linalg.norm(p1m) * np.cos(a1)

        rrx[1][1] = np.cos(-a1)
        rrx[1][2] = -np.sin(-a1)
        rrx[2][1] = np.sin(-a1)
        rrx[2][2] = np.cos(-a1)

        aa = np.dot(ra, rrx.T)

        r1b = np.zeros(3)
        for i in range(3):
            r1b[i] = p1[i] + aa[i][1] * np.linalg.norm(p1m) * np.cos(a1)

        y = origin - p2
        y = self.normalize(y)
        z = np.cross(n, y)
        for i in range(3):
            ra[i][0] = n[i]
            ra[i][1] = y[i]
            ra[i][2] = z[i]

        a2 = np.arcsin(self.radius / np.linalg.norm(p2m))

        rrx[1][1] = np.cos(a2)
        rrx[1][2] = -np.sin(a2)
        rrx[2][1] = np.sin(a2)
        rrx[2][2] = np.cos(a2)

        aa = np.dot(ra, rrx.T)

        r2a = np.zeros(3)
        for i in range(3):
            r2a[i] = p2[i] + aa[i][1] * np.linalg.norm(p2m) * np.cos(a2)

        rrx[1][1] = np.cos(-a2)
        rrx[1][2] = -np.sin(-a2)
        rrx[2][1] = np.sin(-a2)
        rrx[2][2] = np.cos(-a2)

        aa = np.dot(ra, rrx.T)

        r2b = np.zeros(3)
        for i in range(3):
            r2b[i] = p2[i] + aa[i][1] * np.linalg.norm(p2m) * np.cos(a2)

        # determine wrapping tangent points r1 & r2
        r1am = r1a - origin
        r1bm = r1b - origin
        r2am = r2a - origin
        r2bm = r2b - origin

        r1am = self.normalize(r1am)
        r1bm = self.normalize(r1bm)
        r2am = self.normalize(r2am)
        r2bm = self.normalize(r2bm)

        j1 = np.dot(r1am, r2am)
        j2 = np.dot(r1am, r2bm)
        j3 = np.dot(r1bm, r2am)
        j4 = np.dot(r1bm, r2bm)

        pr1 = np.zeros((3,))
        pr2 = np.zeros((3,))
        if j1 > j2 and j1 > j3 and j1 > j4:
            pr1 = r1a
            pr2 = r2a
            r11 = r1b
            r22 = r2b
        elif j2 > j3 and j2 > j4:
            pr1 = r1a
            pr2 = r2b
            r11 = r1b
            r22 = r2a
        elif j3 > j4:
            pr1 = r1b
            pr2 = r2a
            r11 = r1a
            r22 = r2b
        else:
            pr1 = r1b
            pr2 = r2b
            r11 = r1a
            r22 = r2a 

        #check if we have to wrap and how
        if(self.sign != 0):
            #check if p1 and p2 are on the same side as the wrapping side
            if(np.sign(p1[self.axis]) == self.sign or np.sign(p2[self.axis]) == self.sign):
                r_s = self.radius ** 2;
                p_close, t = self.closest_point_to_line(origin, p1, p1p2)
                t = -t
            if(np.sign(p1[self.axis]) != self.sign or np.sign(p2[self.axis]) != self.sign):
                # Initialize arrays and variables
                wrapaxis = np.zeros(3)
                sum_musc = np.zeros(3)
                sum_r = np.zeros(3)

                # Determine wrapaxis
                for i in range(3):
                    wrapaxis[i] = self.sign if i == self.axis else 0.0

                # Determine best constrained r1 & r2 tangent points
                sum_musc = (origin - p1) + (origin - p2)

                # Normalize sum_musc or set it to zero
                sum_musc = np.where(np.linalg.norm(sum_musc) == 0, np.zeros(3), sum_musc / np.linalg.norm(sum_musc))

                if np.dot(r1a, sum_musc) > np.dot(r1b, sum_musc):
                    pr1 = r1a
                    r11 = r1b
                else:
                    pr1 = r1b
                    r11 = r1a

                if np.dot(r2a, sum_musc) > np.dot(r2b, sum_musc):
                    pr2 = r2a
                    r22 = r2b
                else:
                    pr2 = r2b
                    r22 = r2a

                # Flip if necessary
                sum_musc = (pr1 - p1) + (pr2 - p2)

                # Normalize sum_musc or set it to zero
                sum_musc = self.normalize(sum_musc)
                if np.dot(sum_musc, wrapaxis) < 0.0:
                    pr1, pr2 = r11, r22

                # Determine if the resulting tangent points create a far side wrap
                sum_musc = (pr1 - p1) + (pr2 - p2)
                sum_r = (pr1 - origin) + (pr2 - origin)

                if np.dot(sum_r, sum_musc) < 0.0:
                    far_side_wrap = True

        wrap_pts = []
        # Initialize arrays and variables
        r1m = np.zeros(3)
        r2m = np.zeros(3)
        r1n = np.zeros(3)
        r2n = np.zeros(3)
        # Calculate r1m and r2m
        r1m = pr1 - origin
        r2m = pr2 - origin

        # Normalize r1m and r2m or set them to zero
        r1n = self.normalize(r1m)
        r2n = self.normalize(r2m)

        # Calculate angle
        angle = np.arccos(np.dot(r1n, r2n))

        if far_side_wrap:
            angle = -(2 * np.pi - angle)

        r1r2 = self.radius * angle
        self.path_length = r1r2

        axis3 = np.cross(r1n, r2n)
        axis3 = self.normalize(axis3)
        axis = np.zeros((4, 1))
        for ii in range(3):
            axis[ii] = axis3[ii]
        axis[3] = 1.0

        # Calculate the number of wrap segments
        numWrapSegments = int(self.path_length / 0.002)
        if numWrapSegments < 1:
            numWrapSegments = 1

        vec = np.array([r1m[0], r1m[1], r1m[2]])
        self.wrap_pts.append(pr1)
        for i in range(numWrapSegments - 2):
            wangle = np.radians(angle * (i + 1) / (numWrapSegments - 1) * 180.0 / np.pi)  # Convert to degrees
            # Calculate the rotation matrix R
            axis_unit = axis[:3] / np.linalg.norm(axis[:3])
            c = np.cos(wangle)
            s = np.sin(wangle)
            t = 1 - c

            R = np.array([
                [t * axis_unit[0]**2 + c, t * axis_unit[0] * axis_unit[1] - s * axis_unit[2], t * axis_unit[0] * axis_unit[2] + s * axis_unit[1]],
                [t * axis_unit[0] * axis_unit[1] + s * axis_unit[2], t * axis_unit[1]**2 + c, t * axis_unit[1] * axis_unit[2] - s * axis_unit[0]],
                [t * axis_unit[0] * axis_unit[2] - s * axis_unit[1], t * axis_unit[1] * axis_unit[2] + s * axis_unit[0], t * axis_unit[2]**2 + c]
            ])
            rotvec = np.dot(np.reshape(R, (3, 3)), vec)

            wp = origin + rotvec
            self.wrap_pts.append(wp)
        self.wrap_pts.append(pr2)

    def draw(self, model, data, viz, points):
        # Create a cylinder and add it to the visualizer
        sphere = g.Sphere(self.radius)
        viz.viewer[self.name].set_object(sphere)#, g.MeshBasicMaterial(color=0x0022ff, wireframe=True, reflectivity=0.2))

        # Set the cylinder's pose (position and orientation)
        pos = data.oMf[model.getFrameId(self.name)]
        T = np.r_[np.c_[pos.rotation, pos.translation], [[0, 0, 0, 1]]]
        viz.viewer[self.name].set_transform(T)
        
        count = 0
        for p in self.wrap_pts:
            temp = data.oMf[model.getFrameId(self.name)].act(SE3(self.position.rotation, np.array(p)))
            points.append(temp.translation)
            # sphere = g.Sphere(0.002)
            # viz.viewer["w" + str(count)].set_object(sphere)
            # T = np.r_[np.c_[temp.rotation, temp.translation], [[0, 0, 0, 1]]]
            # viz.viewer["w" + str(count)].set_transform(T)
            # count += 1

        return points

    def is_object_on_path(self, p1, p2, model, data):
        pose = data.oMf[model.getFrameId(self.parent)].act(self.position)
        center = pose.translation
        p1_p2 = p2 - p1
        d = np.linalg.norm(p1_p2)
        p1_p2 /= d
        # Calculate the vector from the cylinder's center to point1
        vector_to_point1 = center - p1
        # Calculate the projection of vector_to_p1 onto the cylinder axis
        proj = np.dot(vector_to_point1, p1_p2)
        if proj <= d:
            closest_point = p1 + proj * p1_p2
            if np.linalg.norm(closest_point - center) <= self.radius:
                return True
        return False
    
class WrapCylinder(WrapObject): 
    def __init__(self, position, parent_joint, model, axis, sign, name, muscle_name, radius, height, cyl_axis):
        super().__init__(position, parent_joint, model, axis, sign, name, muscle_name)

        self.radius = radius
        self.height = height

        self.p0 = np.zeros((3,))
        self.p0[cyl_axis] = -1
        self.dn = np.zeros((3,))
        self.dn[cyl_axis] = 1

        self.cyl_axis = cyl_axis

    def compute_tangent_point(self, p):
        p1, _ = self.closest_point_to_line(p, self.p0, self.dn)

        vv = p - p1
        p1_d = np.linalg.norm(vv)
        vv = self.normalize(vv)
        sin_theta = self.radius / p1_d
        
        d = self.radius * sin_theta
        pp = p1 + d * vv

        dist = np.sqrt(self.radius**2 - d**2)
        uu = np.cross(self.dn, vv)

        ra = pp + dist * uu
        rb = pp - dist * uu

        return ra, rb, p1, p1_d

    def compute_path(self, p1, p2):
        far_side_wrap = False
        long_wrap = False
        self.wrap_pts = []
        if self.squared_distance_point_line(p1, self.p0, self.dn) < self.radius**2 or self.squared_distance_point_line(p2, self.p0, self.dn) < self.radius**2:
            return self.radius
        
        cyl_start = -0.5 * self.height * self.dn 
        cyl_end = 0.5 * self.height * self.dn
        near12 = near00 = np.zeros((3,))
        intersect, near12, near00, t12, t00 = self.intersect_lines(p1, p2, cyl_start, cyl_end)
        
        if not intersect:
            self.path_length = 0
            return 
        
        if self.axis != 0:
            if np.linalg.norm(near12 - near00) < self.radius and t12 > 0 and t12 < 1.0:
                pass
            else:
                self.path_length = np.linalg.norm(p1 - p2)
                return 

        #Tangent point candidates r1a & r1b
        r1a, r1b, p11, p11_d = self.compute_tangent_point(p1)
        # Tangent point candidates r2a & r2b
        r2a, r2b, p22, p22_d = self.compute_tangent_point(p2)
        # Choose best tangent points
        if self.sign != 0:
            if(np.sign(p1[self.axis]) == self.sign or np.sign(p2[self.axis]) == self.sign):
                if np.linalg.norm(near12 - near00)**2 < self.radius**2 and t12 > 0 and t12 < 1.0:
                    pass
                else:
                    if(np.sign(p1[self.axis]) != np.sign(p2[self.axis]) and np.sign(near12[self.axis]) != self.sign):
                        pass
                    else:
                        self.path_length = 0
                        return

            r1am = r1a - p11
            r1bm = r1b - p11
            r2am = r2a - p22
            r2bm = r2b - p22

            alpha = np.arccos(np.dot(r1am, r2bm) / (np.linalg.norm(r1am) * np.linalg.norm(r2bm)))
            beta = np.arccos(np.dot(r1bm, r2am) / (np.linalg.norm(r1bm) * np.linalg.norm(r2am)))
        
            if(np.sign(r1a[self.axis]) == self.sign and np.sign(r1b[self.axis]) == self.sign):
                if(np.sign(r2a[self.axis]) == self.sign):
                    pr1 = r1b
                    pr2 = r2a
                    if alpha > beta:
                        far_side_wrap = False
                    else:
                        far_side_wrap = True 
                else:
                    pr1 = r1a
                    pr2 = r2b
                    if alpha > beta:
                        far_side_wrap = True
                    else:
                        far_side_wrap = False
            elif(np.sign(r1a[self.axis]) == self.sign and np.sign(r1b[self.axis]) != self.sign):
                pr1 = r1a-1
                pr2 = r2b
                if alpha > beta:
                    far_side_wrap = True
                else:
                    far_side_wrap = False
            elif(np.sign(r1a[self.axis]) != self.sign and np.sign(r1b[self.axis]) == self.sign):
                pr1 = r1b
                pr2 = r2a
                if alpha > beta:
                    far_side_wrap = False
                else:
                    far_side_wrap = True
            elif(np.sign(r1a[self.axis]) != self.sign and np.sign(r1b[self.axis]) != self.sign):
                if(np.sign(r2a[self.axis]) == self.sign):
                    pr1 = r1b
                    pr2 = r2a
                    if alpha > beta:
                        far_side_wrap = False
                    else:
                        far_side_wrap = True
                elif(np.sign(r2b[self.axis]) == self.sign):
                    pr1 = r1a
                    pr2 = r2b
                    if alpha > beta:
                        far_side_wrap = True
                    else:
                        far_side_wrap = False
                else:
                    if alpha > beta:
                        pr1 = r1a
                        pr2 = r2b
                        far_side_wrap = True
                    else:
                        pr1 = r1b
                        pr2 = r2a
                        far_side_wrap = True

            # Check if it's a short wrap (less than halpf the cylinder)
            sum_musc = (pr1 - p1) + (pr2 - p2)
            sum_r = (pr1 - p11) + (pr2 - p22)

            if(np.dot(sum_r, sum_musc)) < 0:
                long_wrap = True

        else:

            r1am = self.normalize(r1a - p11)
            r1bm = self.normalize(r1b - p11)
            r2am = self.normalize(r2a - p22)
            r2bm = self.normalize(r2b - p22)

            dot1 = np.dot(r1am, r2am)
            dot2 = np.dot(r1am, r2bm)
            dot3 = np.dot(r1bm, r2am)
            dot4 = np.dot(r1bm, r2bm)

            if(dot1 > dot2 and dot1 > dot3 and dot1 > dot4):
                pr1 = r1a
                pr2 = r2a
            elif(dot2 > dot3 and dot2 > dot4):
                pr1 = r1a
                pr2 = r2b
            elif(dot3 > dot4):
                pr1 = r1b
                pr2 = r2a
            else:
                pr1 = r1b
                pr2 = r2b

        # bisect angle between r1 and r2 vectors
        uu = pr1 - p11
        vv = self.normalize(pr2 - p22 + uu)

        #find the apex point 
        t = p11_d / (p11_d + p22_d)
        #find it on the muscle line
        muscle_point = p1 + t * (p2 - p1)

        ax_pt, t = self.closest_point_to_line(muscle_point, self.p0, self.dn)  

        #normal plane through p1, p2, and ax_pt
        l1 = self.normalize(p1 - ax_pt)
        l2 = self.normalize(p2 - ax_pt)

        plane_normal = self.normalize(np.cross(l1, l2))

        vert1 = self.normalize(np.cross(self.dn, plane_normal))
        vert2 = self.normalize(np.cross(plane_normal, self.dn))

        apex1 = ax_pt + self.radius * vert1
        apex2 = ax_pt + self.radius * vert2
        
        d1 = np.linalg.norm(muscle_point - apex1)
        d2 = np.linalg.norm(muscle_point - apex2)

        if far_side_wrap:
            if(d1 < d2):
                apex = apex2
            else:
                apex = apex1
        else:
            if(d1 < d2):
                apex = apex1
            else:
                apex = apex2
        
        uu = self.normalize(p1 - apex)
        vv = self.normalize(p2 - apex)

        plane_normal = self.normalize(np.cross(uu, vv))
        d = -p1[0]*plane_normal[0]-p1[1]*plane_normal[1]-p1[2]*plane_normal[2]

        r1a = pr1 - 10*self.dn
        r2a = pr2 - 10*self.dn
        r1b = pr1 + 10*self.dn
        r2b = pr2 + 10*self.dn

        f1, r1_inter = self.intersect_line_plane(r1a, r1b, plane_normal, d)
        f2, r2_inter = self.intersect_line_plane(r2a, r2b, plane_normal, d)

        if f1:
            r1a, t = self.closest_point_to_line(r1_inter, p11, p22)
            if(np.linalg.norm(r1a- p22) < np.linalg.norm(p11 - p22)):
                pr1 = r1_inter

        if f2:
            r2a, t = self.closest_point_to_line(r2_inter, p11, p22)
            if(np.linalg.norm(r2a - p22) < np.linalg.norm(p11 - p22)):
                pr2 = r2_inter

        halfL = self.height / 2.0
        if (pr1[self.cyl_axis] < -halfL or pr1[self.cyl_axis] > halfL) and (pr2[self.cyl_axis] < -halfL or pr2[self.cyl_axis] > halfL):
            self.path_length = 0
            return 

        self.make_wrap(p1, p2, long_wrap, far_side_wrap, pr1, pr2)

    def make_wrap(self, p1, p2, long_wrap, far_side_wrap, pr1, pr2):
        sense = -1 if far_side_wrap else 1
        p_res1 = pr1
        p_res2 = pr2
        num_wrap_seg = 1
        i = 0
        while(i < num_wrap_seg):
            r1a, t = self.closest_point_to_line(p_res1, self.p0, self.dn)
            r2a, t = self.closest_point_to_line(p_res2, self.p0, self.dn)

            axial_d = np.dot(r2a - r1a, self.dn)

            uu = p_res1 - r1a
            vv = p_res2 - r2a

            theta = np.arctan2(np.dot(np.cross(uu, vv), self.dn), np.dot(uu, vv))
            if(far_side_wrap):
                theta = 2.0 * np.pi - theta

            x = self.radius * theta
            y = axial_d

            self.path_length = np.sqrt(x**2 + y**2)
            num_wrap_seg = int(self.path_length / 0.002)
            if(num_wrap_seg < 1):
                num_wrap_seg = 1
            
            # Test if tangent point need to be adjusted 
            
            t = float(i / num_wrap_seg)
            r = R.from_rotvec(sense*t*theta*self.dn).as_matrix()
            pt = np.dot(r, p_res1)  + self.dn * (t * axial_d)
            adjusted1, pr1_, w_pt1 = self.adjust_point(p1, p_res1, pt)
            
            r = R.from_rotvec(sense*(1.0 - t)*theta*self.dn).as_matrix()
            pt2 = np.dot(r, p_res1)  + self.dn * ((1.0 - t) * axial_d)
            adjusted2, pr2_, w_pt2 = self.adjust_point(p2, p_res2, pt2)
            if(adjusted1 or adjusted2) and i == 1:
                p_res1 = pr1_
                p_res2 = pr2_
                i = 0
                self.wrap_pts = []
                continue
            else:
                if(i == 0):
                    self.wrap_pts.append(p_res1)
                self.wrap_pts.append(pt)
            i += 1
        self.wrap_pts.append(p_res2)

    def adjust_point(self, p, pr, w_p):
        pr_vec = self.normalize(pr - p)
        rw_vec = self.normalize(w_p - pr)
        
        adjust = False
        
        alpha = np.arccos(np.dot(pr_vec, self.dn))
        omega = np.arccos(np.dot(rw_vec, self.dn))
        r1a = np.zeros((3,))
        w1a = np.zeros((3,))
        temp = np.zeros((3,))
        if(np.abs(alpha - omega) > 15 * np.pi / 180):
            r1a, t = self.closest_point_to_line(p, pr, self.dn)
            w1a, t = self.closest_point_to_line(w_p, pr, self.dn)
            intersect, pw_p, r1aw1a, t12, t00 = self.intersect_lines(p, w_p, r1a, w1a)
            temp = (r1aw1a - pr)
    
            adjust = True
        return adjust, pr + 1.5 * temp, w1a

    def draw(self, model, data, viz, points):
        #Create a cylinder and add it to the visualizer
        cylinder = g.Cylinder(self.height, self.radius*0.9)
        viz.viewer[self.name].set_object(cylinder)#, g.MeshBasicMaterial(color=0x0022ff, wireframe=True, reflectivity=0.2))

        # Set the cylinder's pose (position and orientation)
        pos = data.oMf[model.getFrameId(self.name)]
        T = np.r_[np.c_[pos.rotation, pos.translation], [[0, 0, 0, 1]]]
        viz.viewer[self.name].set_transform(T)

        count = 0
        for p in self.wrap_pts:
            temp = data.oMf[model.getFrameId(self.name)].act(SE3(self.position.rotation, np.array(p)))
            points.append(temp.translation)
            # sphere = g.Sphere(0.002)
            # viz.viewer["w" + str(count)].set_object(sphere)
            # T = np.r_[np.c_[temp.rotation, temp.translation], [[0, 0, 0, 1]]]
            # viz.viewer["w" + str(count)].set_transform(T)
            # count += 1

        return points

    def is_object_on_path(self, p1, p2, model, data):
        pose = data.oMf[model.getFrameId(self.name)]
        cylinder_center = pose.translation
        cylinder_axis = np.dot(pose.rotation, np.array([0, 0, 1]).T) 
        cylinder_axis /= np.linalg.norm(cylinder_axis)
        # Calculate the vector from the cylinder's center to point1
        vector_to_point1 = p1 - cylinder_center
        # Calculate the projection of vector_to_p1 onto the cylinder axis
        projection_point1 = np.dot(vector_to_point1, cylinder_axis)
        # Calculate the vector from the cylinder's center to point2
        vector_to_point2 = p2 - cylinder_center
        # Calculate the projection of vector_to_point2 onto the cylinder axis
        projection_point2 = np.dot(vector_to_point2, cylinder_axis)
        # Check if the projections of both points are within the height range of the cylinder
        if self.height / 2 <= projection_point1 <= self.height / 2 or -self.height/ 2 <= projection_point2 <= self.height / 2:
            p1_p2 = p2 - p1
            d= np.linalg.norm(p1_p2)
            p1_p2 /= d
            proj = np.dot(-vector_to_point1, p1_p2)
            if proj <= d:
                
                projected_point = p1 + proj * p1_p2
                distance = np.linalg.norm(projected_point - cylinder_center)
                # Check if the distance is less than the cylinder's radius
                if distance <= self.radius:
                    return True
        return False