import numpy as np
import pinocchio as pin

class MuscleGroup:
    def __init__(self):
        self.group = []
        self.names = []

    def add(self, m):
        self.group.append(m)
        self.names.append(m.name)

    def add_wrapping_object(self, objects):
        for k in range(len(self.group)):
            for o in objects:
                if self.group[k].name in o.muscle:
                    self.group[k].geom.objects.append(o)


    def update_geometry(self, model, data, q, viz):
        for k in range(len(self.group)):
            self.group[k].geom.update_geometry(model, data, q, viz, self.names[k])

    def update_all(self, u, model, data):
        self.full_length_jacobian = np.zeros((len(self.group), model.nq))
        forces = np.zeros((len(self.group),))
        for k in range(len(self.group)):
            m = self.group[k]
            forces[k] = m.update_muscle(u[m.name])

            self.full_length_jacobian[k, :] = m.geom.length_jacobian

        return (-self.full_length_jacobian.T @ forces)
    # def update_all(self, u, model, data):
    #     forces = [pin.Force.Zero() for k in range(model.nq + 1)]
        
    #     for k in range(len(self.group)):
    #         f_o = pin.Force.Zero()
    #         f_i = pin.Force.Zero()
    #         m = self.group[k]
    #         temp_origin, temp_insertion = m.update_muscle(u[m.name])
    #         f_o.linear = temp_origin
    #         f_i.linear = temp_insertion

    #         #find index of origin of muscle
    #         o = model.getJointId(m.geom.joints[0])
    #         # forces[o] += data.oMi[o].actInv(data.oMf[model.getFrameId(m.geom.points[0].name)].act(f_o))
    #         i = model.getJointId(m.geom.joints[1])
    #         forces[i] += data.oMi[i].actInv(data.oMf[model.getFrameId(m.geom.points[-1].name)].act(f_i))

    #     return forces
    
    def reinitialize(self):
        for m in self.group:
            m.reinitialize()
